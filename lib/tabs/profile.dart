import 'package:flutter/material.dart';

import 'package:edukasi_parent/widgets/widgets.dart';

class ProfilePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final Size media = MediaQuery.of(context).size;
    return Scaffold(
      appBar: _buildAppBar(media),
      body: _buildBody(media),
    );
  }

  Widget _buildAppBar(Size media) {
    final bool target = media.height > 640;
    return PreferredSize(
      preferredSize:
          Size.fromHeight(target ? 116.0 : 108.0),
      child: AppBar(
        brightness: Brightness.light,
        elevation: 0.0,
        backgroundColor: Colors.white,
        flexibleSpace: _buildTopSpace(media),
      ),
    );
  }

  Widget _buildTopSpace(Size media) {
    final bool target = media.height > 640;
    return SafeArea(
      child: Container(
        padding: EdgeInsets.only(left: 16.0, top: target ? 32.0 : 24.0),
        decoration: BoxDecoration(
            border: Border(
          bottom: BorderSide(color: Colors.grey[300], width: 1.0),
        )),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            BluePurpleGradientText(
                text: 'Profile', fontSize: target ? 32.0 : 28.0),
            SizedBox(height: 8.0),
            Text(
              'Zalina Raine Wyllie',
              style: TextStyle(
                fontSize: 16.0,
                color: Color(0xFF989B9C),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildBody(Size media) {
    return Center(
      child: Text(
        'Profile Page',
        style: TextStyle(fontSize: 16.0, color: Colors.grey),
      ),
    );
  }
}
