import 'package:flutter/material.dart';

import 'package:edukasi_parent/models/models.dart';
import 'package:edukasi_parent/widgets/widgets.dart';
import 'package:edukasi_parent/pages/contact.dart';

class InboxPage extends StatelessWidget {
  final _inboxList = [
    Inbox(
        teacherName: 'Latika Puspasari',
        teacherSubject: 'Homeroom Teacher',
        urlPhoto:
            'https://image.shutterstock.com/image-photo/close-portrait-smiling-brunette-woman-260nw-530446444.jpg',
        lastMessage: 'Sama-sama ibu.',
        timestamp: '1d'),
    Inbox(
        teacherName: 'Vanya Sitorus',
        teacherSubject: 'Math Teacher',
        urlPhoto:
            'https://image.shutterstock.com/image-photo/portrait-young-beautiful-cute-cheerful-260nw-666258808.jpg',
        lastMessage: 'Baik, sama2 bu',
        timestamp: '15 Mar'),
    Inbox(
        teacherName: 'Natalia Napitupulu',
        teacherSubject: 'English Teacher',
        urlPhoto:
            'https://image.shutterstock.com/image-photo/headshot-portrait-happy-ginger-girl-260nw-623804987.jpg',
        lastMessage: 'Terimakasih kembali',
        timestamp: '15 Mar')
  ];

  @override
  Widget build(BuildContext context) {
    final Size media = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: _buildAppBar(context, media),
      body: _buildInboxList(_inboxList),
    );
  }

  Widget _buildAppBar(BuildContext context, Size media) {
    final bool target = media.height > 640;
    return PreferredSize(
      preferredSize:
          Size.fromHeight(target ? 135.0 : 130.0),
      child: AppBar(
        brightness: Brightness.light,
        elevation: 0.0,
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(color: Colors.black),
        flexibleSpace: _buildTopSpace(context, media),
      ),
    );
  }

  Widget _buildTopSpace(BuildContext context, Size media) {
    final bool target = media.height > 640;
    return SafeArea(
      child: Container(
        padding: EdgeInsets.only(left: 16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              alignment: Alignment.topRight,
              margin: EdgeInsets.only(right: 16.0),
              child: IconButton(
                icon: Icon(Icons.add),
                onPressed: () => Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => ContactPage()),
                    ),
              ),
            ),
            BluePurpleGradientText(
                text: 'Inbox', fontSize: target ? 32.0 : 28.0),
            SizedBox(height: 8.0),
            Text(
              'You have 1 unread message',
              style: TextStyle(
                fontSize: 16.0,
                color: Color(0xFF989B9C),
              ),
            ),
            SizedBox(height: 16.0)
          ],
        ),
      ),
    );
  }

  Widget _buildInboxList(List<Inbox> inbox) {
    return ListView.builder(
      key: PageStorageKey('inbox_list'),
      shrinkWrap: true,
      itemCount: inbox.length,
      itemBuilder: (context, index) => InboxList(index, inbox: inbox[index]),
    );
  }
}
