import 'package:flutter/material.dart';
import 'package:edukasi_parent/utils/custom_icons.dart';
import 'package:edukasi_parent/widgets/widgets.dart' as widget;
import 'package:edukasi_parent/tabs/tabs.dart';

// TODO: Doc inline
class StartPage extends StatefulWidget {
  @override
  _StartPageState createState() => _StartPageState();
}

class _StartPageState extends State<StartPage> {
  int _currentIndex = 0;
  final _pages = [
    ActivityPage(),
    WhatsDuePage(),
    InboxPage(),
    UpdatePage(),
    ProfilePage()
  ];

  @override
  Widget build(BuildContext context) {
    final media = MediaQuery.of(context).size;
    return Scaffold(
        body: _pages[_currentIndex],
        bottomNavigationBar: _buildBottomNavigationBar(media));
  }

  Widget _buildBottomNavigationBar(Size media) {
    return widget.BottomNavigationBar(
      currentIndex: _currentIndex,
      type: widget.BottomNavigationBarType.fixed,
      onTap: (int index) => setState(() => _currentIndex = index),
      onLongPress: (int index) => index == 4
          ? _buildModalSwitchStudent()
          : setState(() => _currentIndex = index),
      iconSize: media.height > 640.0 ? 24.0 : 20.0,
      items: [
        BottomNavigationBarItem(
            icon: Icon(CustomIcons.icon_activity, color: Color(0xFFA7AAAC)),
            activeIcon: Icon(CustomIcons.icon_activity,
                color: Theme.of(context).primaryColor),
            title: Text('')),
        BottomNavigationBarItem(
            icon: Icon(CustomIcons.icon_duedate, color: Color(0xFFA7AAAC)),
            activeIcon: Icon(CustomIcons.icon_duedate,
                color: Theme.of(context).primaryColor),
            title: Text('')),
        BottomNavigationBarItem(
            icon: Icon(CustomIcons.icon_inbox, color: Color(0xFFA7AAAC)),
            activeIcon: Icon(CustomIcons.icon_inbox,
                color: Theme.of(context).primaryColor),
            title: Text('')),
        BottomNavigationBarItem(
            icon:
                Icon(CustomIcons.icon_notifications, color: Color(0xFFA7AAAC)),
            activeIcon: Icon(CustomIcons.icon_notifications,
                color: Theme.of(context).primaryColor),
            title: Text('')),
        BottomNavigationBarItem(
            icon: Container(
              width: 26.0,
              height: 26.0,
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                image: DecorationImage(
                  fit: BoxFit.cover,
                  image: NetworkImage(
                      'https://f4m6r3s3.stackpathcdn.com/wp-content/uploads/2018/11/gempi-696x391.jpg'),
                ),
              ),
            ),
            title: Text('')),
      ],
    );
  }

  Future _buildModalSwitchStudent() {
    return widget.showCustomModalBottomSheet(
        context: context,
        builder: (context) {
          final media = MediaQuery.of(context).size;
          return Container(
            color: Color(0xFF737373),
            // TODO: Add flexiblity when more than 2 students
            height: media.height / 3,
            child: GestureDetector(
              behavior: HitTestBehavior.opaque,
              onTap: () {},
              child: Container(
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(10.0),
                    topRight: Radius.circular(10.0),
                  ),
                ),
              ),
            ),
          );
        });
  }
}
